const express = require("express");
const app = express();
const mongoose = require("mongoose");
const morgan = require("morgan");

app.use(express.json());
app.use(morgan("dev"));

mongoose.connect(
  "mongodb://127.0.0.1:27017/logs",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  () => console.log("MongoDB Connected ! ")
);

// var ip = "60.243.31.80";

app.use("/api/logs", require("./routes/log.routes"));

app.use("*", (req, res) => {
  res.status(404).json({ message: "URL Not found" });
});

app.use((err, req, res, next) => {
  console.log(err);
  res.status(500).json({
    message: err,
  });
});

app.listen(process.env.PORT || 3001, () => console.log("Server running !!"));

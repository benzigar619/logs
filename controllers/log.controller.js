const geoip = require("geoip-lite");
const Log = require("../models/log.model");

module.exports = {
  get: async (req, res, next) => {
    try {
      const total = await Log.count();
      const logs = await Log.aggregate([
        { $group: { _id: "$url", count: { $sum: 1 } } },
        { $project: { url: "$_id", count: "$count", _id: 0 } },
        { $skip: req.skip },
        { $limit: req.limit },
      ]);
      res.json({
        total,
        response: logs,
      });
    } catch (err) {
      next(err);
    }
  },
  projects: async (req, res, next) => {
    try {
      const projects = await Log.aggregate([
        { $group: { _id: "$project", count: { $sum: 1 } } },
        { $project: { _id: 0, project: "$_id", api: "$count" } },
        { $skip: req.skip },
        { $limit: req.limit },
      ]);
      res.json(projects);
    } catch (err) {
      next(err);
    }
  },
  analytics: async (req, res, next) => {
    try {
      const match = {};
      match.project = req.body.project;
      if (req.body.city) match.city = req.body.city;
      if (req.body.country) match.country = req.body.country;

      const count = await Log.aggregate([
        { $match: match },
        {
          $group: {
            _id: "$url",
            requests: { $sum: 1 },
            ip: { $addToSet: "$ip" },
            country: { $addToSet: "$country" },
            city: { $addToSet: "$city" },
          },
        },
      ]);
      const urls = await Log.aggregate([
        { $match: match },
        {
          $group: {
            _id: "$url",
            requests: { $sum: 1 },
            ip: { $addToSet: "$ip" },
            country: { $addToSet: "$country" },
            city: { $addToSet: "$city" },
          },
        },
        { $project: { _id: 0, url: "$_id", requests: 1, uniqueVisitors: { $size: "$ip" }, country: 1, city: 1 } },
        { $skip: req.skip },
        { $limit: req.limit },
      ]);
      res.json({
        count: count.length,
        response: urls,
      });
    } catch (err) {
      next(err);
    }
  },
  create: async (req, res, next) => {
    try {
      const { url, method, ip, project, hostName } = req.body;
      var geo = geoip.lookup(ip);
      const data = await Log.create({
        hostname: hostName,
        project,
        ip: ip === "" ? "unknown" : ip,
        method,
        url,
        country: (geo && geo.country) || "unknown",
        region: (geo && geo.region) || "unknown",
        timezone: (geo && geo.timezone) || "unknown",
        latLng: (geo && geo.ll) || [0, 0],
        city: (geo && geo.city) || "unknown",
      });
      res.json(data);
    } catch (err) {
      next(err);
    }
  },
};

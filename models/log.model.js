const mongoose = require("mongoose");

const LogSchema = new mongoose.Schema(
  {
    hostname: String,
    project: String,
    ip: String,
    method: String,
    url: String,
    country: String,
    region: String,
    timezone: String,
    latLng: [Number],
    city: String,
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

module.exports = mongoose.model("Log", LogSchema);

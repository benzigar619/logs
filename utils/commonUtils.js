const { validationResult } = require("express-validator");

module.exports = {
  skipLimitCheck: (req, res, next) => {
    req.skip = req.body.skip || 0;
    req.limit = req.body.limit || 10;
    next();
  },
  catchValidatorError: (req, res, next) => {
    const errorFormatter = ({ location, msg, param, value, nestedErrors }) => {
      return `${location} | ${param} | ${msg}`;
    };
    const errors = validationResult(req).formatWith(errorFormatter);
    if (!errors.isEmpty()) {
      res.status(422).json({ errors: errors.array() });
    } else next();
  },
};

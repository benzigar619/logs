const { body } = require("express-validator");
module.exports = {
  create: [
    body("url").exists().withMessage("Required").isLength({ min: 1 }).withMessage("Minimum Length 1"),
    body("method")
      .exists()
      .withMessage("Required")
      .isIn(["GET", "POST", "PUT", "DELETE"])
      .withMessage("GET POST PUT DELETE is only supported"),
    body("ip").exists().withMessage("Required"),
    body("project").exists().withMessage("Required").isLength({ min: 1 }).withMessage("Should not be empty"),
    body("hostName").exists().withMessage("Required").isLength({ min: 1 }).withMessage("Should not be empty"),
  ],
  analytics: [body("project").exists().withMessage("Required")],
};

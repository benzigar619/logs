const express = require("express");
const controller = require("../controllers/log.controller");
const utils = require("../utils/commonUtils");
const validator = require("../validators/log.validator");

const router = express.Router();

router.get("/url", utils.skipLimitCheck, controller.get);
router.get("/projects", utils.skipLimitCheck, controller.projects);
router.post("/create", validator.create, utils.catchValidatorError, controller.create);
router.post("/analytics", validator.analytics, utils.catchValidatorError, utils.skipLimitCheck, controller.analytics);

module.exports = router;
